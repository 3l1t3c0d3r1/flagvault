package com.example.flagvault;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.View;

import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DbHelper dbHelper = new DbHelper(this, "vault", null, 1);

        try {
            dbHelper.installDatabaseFromAssets();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void onDecryptClicked(View view) {
        String flag = null;
        try {
            flag = decryptFlag();
            ((TextView) findViewById(R.id.flagText)).setText(flag);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            ((TextView) findViewById(R.id.flagText)).setText("Wrong key");
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    protected String decryptFlag() throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] password = ((EditText) findViewById(R.id.passwordTextEdit)).getText().toString().getBytes("UTF-8"); /*Erase later: JY6M4rN78wAFN4aU*/
        byte[] cipherBytes;

        if (password.length == 0) {
            throw new InvalidKeyException("Invalid key");
        }

        SQLiteDatabase database = SQLiteDatabase.openDatabase(getDatabasePath("vault").getPath(), null, 0);
        Cursor cursor = database.rawQuery("SELECT value FROM secrets WHERE key = 'flag'", null);
        if (cursor.moveToFirst()) {
            cipherBytes = Base64.decode(cursor.getString(0),0);
            Log.d("Decrypt", "Decryption of " + cursor.getString(0) + " with " + new String(password, StandardCharsets.UTF_8));
        } else {
            return "An error has occurred";
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(password, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] decryptedFlag = cipher.doFinal(cipherBytes);
        String flag = new String(decryptedFlag, StandardCharsets.UTF_8);
        return flag;
    }
}