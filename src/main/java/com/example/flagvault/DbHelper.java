package com.example.flagvault;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DbHelper extends SQLiteOpenHelper {

    private Context context;
    private String databaseName;
    private SharedPreferences preferences;

    private boolean databaseIsInstalled() {
        return preferences.getBoolean(databaseName, false);
    }

    public void installDatabaseFromAssets() throws IOException {
        Log.d("DB", "Installing DB...");

//        if (databaseIsInstalled()) {
//            Log.d("DB", "DB already installed. Skipping installation process...");
//            return;
//        }

        InputStream inputStream = context.getAssets().open(databaseName + ".db");

        try {
            File outputFile = new File(context.getDatabasePath(databaseName).getPath());
            FileOutputStream outputStream = new FileOutputStream(outputFile);

            byte[] buf = new byte[8192];
            int length;
            while ((length = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, length);
            }

            inputStream.close();

            outputStream.flush();
            outputStream.close();

            preferences.edit().putBoolean(databaseName, true).apply();
            Log.d("DB", "DB installed !");
        } catch (Throwable exception) {
            throw new RuntimeException("The " + databaseName + "database couldn't be installed.", exception);
        }
    }

    public DbHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
        this.databaseName = name;
        this.preferences = context.getSharedPreferences(context.getPackageName() + ".database_versions", Context.MODE_PRIVATE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
